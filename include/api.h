#ifndef MY_CHAT_API__H_
#define MY_CHAT_API__H_

enum StatusCode {
   UNKNOWN = -1, 
   SUCCESS = 0, 
   WRONG_PASSWORD = 1, 
   NOT_REGISTERED = 2, 
   ALREADY_LOGGED_IN = 3, 
   ALREADY_REGISTERED = 4, 
   INVALID_FORM = 5,//json неправильный
   INVALID_QUERY = 6,//Если ошибка в такой строке GET /login?param1=value1&param2=value2 HTTP/1.1 (при logout и switch channel будет использоваться)
   UNAUTHORIZED = 7 //Если пытаемся выполнить действие, не залогинившись при этом. 
};

enum Method { GET, POST, PUT, DELETE };
/*
cоответствуют Api нашего сервера. Те методы, которые могут быть в заголовке запроса. А какие это могут быть? login, register, 
message
может быть path: /login 
*/
enum Path { LOGIN, REGISTER, MESSAGE, LOGOUT, SWITCH_CHANNEL };

#define UNKNOWN_ID 0

#endif
