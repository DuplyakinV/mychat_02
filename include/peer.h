#ifndef MY_CHAT_PEER__H__
#define MY_CHAT_PEER__H__

#include <string>

struct Peer {
  std::string login;
  std::string password;
  std::string email;
  
  Peer(
    const std::string& login,
    const std::string& password,
    const std::string& email);

  virtual ~Peer();
};

#endif  // PEER__H__

