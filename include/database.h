#ifndef MY_CHAT_DATABASE__H__
#define MY_CHAT_DATABASE__H__

#include "sqlite/sqlite3.h"
#include "peer.h"

/*
 Peers:
  _____________________________________
  |  ID  |  login |  password  |email  |
  |------|--------|------------|-------|
  |  1   |  Alex  |   qqqqqq   |1@y.com|
  |------|--------|------------|-------|
  |  2   | Masha  |  qwerty12  |2@y.com|
  |------|--------|------------|-------|
  |______|________|____________|_______|
 
*/

static const char* DATABASE_NAME = "data.db";

class Database {
public:
  Database();
  virtual ~Database();

  int addPeer(const Peer& peer);
  //Peer readPeer(int id);
  Peer getPeerByLogin(const std::string& login, int& id);
  //void deletePeer(int id);

protected:
  sqlite3* m_db_handler;
  sqlite3_stmt* m_db_statement;
  std::string m_table_name;
  int m_last_id;

  void open();
  void createTable(const std::string& table_name);
  void prepareStatement(const std::string& statement);
  int readLastId(const std::string& i_table_name);
  void close();
};

#endif 

