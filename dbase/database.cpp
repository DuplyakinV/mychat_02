#include <string>
#include "database.h"
#include "logger.h"
#include "all.h"

Database::Database()
  : m_db_handler(nullptr)
  , m_db_statement(nullptr)
  , m_table_name("")
  , m_last_id(1000) {
  open();
}

Database::~Database() {
  close();
}

/* Public API */
// ----------------------------------------------------------------------------
int Database::addPeer(const Peer& peer) {
  
  int id = m_last_id++;

  std::string statement = "INSERT INTO '";
  statement += m_table_name;
  statement += "' VALUES(?1, ?2, ?3, ?4);";
  DBG("Insert statement: %s", statement.c_str());

  prepareStatement(statement);
  // check for SQLITE_OK
  sqlite3_bind_int(m_db_statement, 1, id);  // sample cast
  sqlite3_bind_text(m_db_statement, 2, peer.login.c_str(), peer.login.length(), SQLITE_TRANSIENT);
  sqlite3_bind_text(m_db_statement, 3, peer.password.c_str(), peer.password.length(), SQLITE_TRANSIENT);
  sqlite3_bind_text(m_db_statement, 4, peer.email.c_str(), peer.email.length(), SQLITE_TRANSIENT);
  
  sqlite3_step(m_db_statement);
  sqlite3_finalize(m_db_statement);
  return id;
}


Peer Database::getPeerByLogin(const std::string& login, int& id) {
  std::string statement = "SELECT * FROM '";
  statement += m_table_name;
  statement += "' WHERE LOGIN LIKE '";
  statement += login;
  statement += "';";
  DBG("Read statement: %s", statement.c_str());

  prepareStatement(statement);
  sqlite3_step(m_db_statement);

  id = sqlite3_column_int(m_db_statement, 0);  // check id

  if (id != UNKNOWN_ID) {
    std::string login(reinterpret_cast<const char*>(sqlite3_column_text(m_db_statement, 1)));
    std::string password(reinterpret_cast<const char*>(sqlite3_column_text(m_db_statement, 2)));
    std::string email(reinterpret_cast<const char*>(sqlite3_column_text(m_db_statement, 3)));
    sqlite3_finalize(m_db_statement);
    return Peer(login, password, email);
  }

  sqlite3_finalize(m_db_statement);
  return Peer("", "", "");
}


int Database::readLastId(const std::string& i_table_name) {
  DBG("enter Database::readLastId().");
  std::string statement = "SELECT MAX(ID) FROM '" + i_table_name + "';";
  this->prepareStatement(statement);
  sqlite3_step(this->m_db_statement);
  int last_id = sqlite3_column_int64(this->m_db_statement, 0);
  DBG("Read last id [%d] from table ["%s"].", last_id, i_table_name.c_str());
  //this->__finalize__(statement.c_str()); ---> sqlite3_finalize(this->m_db_statement);
  sqlite3_finalize(this->m_db_statement);
  DBG("exit Database::readLastId().");
  return (last_id);
}

/* Private methods */
// ----------------------------------------------------------------------------
void Database::open() {
  int code = sqlite3_open(DATABASE_NAME, &m_db_handler);
  if (code != SQLITE_OK || m_db_handler == nullptr) {
    ERR("Failed to open database");
  }
  sqlite3_limit(m_db_handler, SQLITE_LIMIT_SQL_LENGTH, 1000000);  // statement limit in bytes
  createTable("Peers");
  //m_last_id = sqlite3_column_int(m_db_statement, 1);  // last id
  m_last_id = readLastId("Peers");
  if (m_last_id == UNKNOWN_ID) {
    m_last_id = 1000; 
  } 
}

void Database::createTable(const std::string& table_name) {
  m_table_name = table_name;

  std::string statement = "CREATE TABLE IF NOT EXISTS ";
  statement += table_name;
  statement += "('ID' INTEGER PRIMARY KEY DEFAULT 0, "
      "'Login' TEXT, "
      "'Password' TEXT, "
      "'Email' TEXT);";

  prepareStatement(statement);
  sqlite3_step(m_db_statement);
  sqlite3_finalize(m_db_statement);
}


void Database::prepareStatement(const std::string& statement) {
  int n_bytes = static_cast<int>(statement.length());
  int code = sqlite3_prepare_v2(m_db_handler, statement.c_str(), n_bytes, &m_db_statement, nullptr);
}

void Database::close() {
  sqlite3_close(m_db_handler);
  sqlite3_free(nullptr);
}

