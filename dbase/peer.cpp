#include "peer.h"

Peer::Peer(
    const std::string& login,
    const std::string& password,
    const std::string& email )
  : login(login)
  , password(password)
  , email(email) {
}

Peer::~Peer() {
}

