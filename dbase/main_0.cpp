#include <vector>
#include <cstdlib>
#include "database.h"
#include "peer.h"
#include "logger.h"

/* Prepare data */
// ----------------------------------------------------------------------------
void generateData(std::vector<Peer>* peers) {
  peers->emplace_back("Vladimir", "Sidorov", "Vladimir@yandex.ru");  // 0
  peers->emplace_back("Sveta", "Sokolova", "Sveta@yandex.ru");  // 1
  peers->emplace_back("Marquiz", "De Sad", "Marquiz@yandex.ru");    // 2
  peers->emplace_back("Petr", "Osipov", "Petr@yandex.ru");       // 3
  peers->emplace_back("Jack", "Sparrow", "Jack@yandex.ru");      // 4
  peers->emplace_back("Coco", "Shanel", "Coco@yandex.ru");     // 5
  peers->emplace_back("Alexander", "Druz", "Alexander@yandex.ru");    // 6
}

/* Work with Database */
// ----------------------------------------------------------------------------
void addPeers(const std::vector<Peer>& peers, Database* database) {
  for (auto& peer : peers) {
    database->addPeer(peer);
  }
}

Peer readRandomPeer(Database* database, int& input_id) {
  input_id = rand() % 7;
  return database->readPeer(input_id);
}

Peer getPeerByLogin(Database* database, std::string& login, long long& input_id) {
  return database->getPeerByLogin(login, input_id);
}

void deletePeer(int id, Database* database) {
  database->deletePeer(id);
}

/* Main */
// ----------------------------------------------------------------------------
int main(int argc, char** argv) {
  DBG("[ChatSever]: Database 0");

  std::vector<Peer> peers;
  generateData(&peers);

  Database database;
  addPeers(peers, &database);

  int id = 0;
  Peer peer = readRandomPeer(&database, id);

  INF("Peer: login=\'%s\', password=\'%s\', email=\'%s\'",
      peer.login.c_str(), peer.password.c_str(), peer.email.c_str());
  
  long long n_id = 0;
  std::string login = "Vladimir";
  Peer peer1 = getPeerByLogin(&database, login, n_id);
  INF("Peer: login=\'%s\', password=\'%s\', email=\'%s\'",
      peer1.login.c_str(), peer1.password.c_str(), peer1.email.c_str());

  // TODO: task 4: Uncomment and check how Database has changed
  // deletePeer(id, &database);

  DBG("[ChatSever]: Database 0 [END]");
  return 0;
}

