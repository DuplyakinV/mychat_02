#ifndef MY_CHAT_SERVER__H_
#define MY_CHAT_SERVER__H_

#include "all.h"

#include "server_api.h"
#include "my_parser.h"

class Server {
public:
  Server(int port);
  void run();
  Request getRequest(int socket, bool* is_closed);
  ~Server();
private:
  bool m_is_stopped;
  int m_socket;

  ServerApi* m_api_impl;
  MyParser m_parser;
  void receiverThread();
  void handleClient(int socket);  

  void stopServer();
  void printHelp();
  bool evaluate(char* command);
};

#endif
