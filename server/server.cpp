#include "server.h"
#include "logger.h"
#include "server_api.h"
#include "my_parser.h"

Method getMethod(const std::string& raw_method);
Path getPath(const std::string& raw_path);

Server::Server(int port)
  : m_is_stopped(false) {
   // 1. socket
   // 2. bind
   // 3. listen

  sockaddr_in address_structure;
  memset(&address_structure, 0 , sizeof(sockaddr_in));
  address_structure.sin_family = AF_INET;
  address_structure.sin_addr.s_addr = htonl(INADDR_ANY);
  address_structure.sin_port = htons(port);
  
  m_socket = socket(PF_INET ,SOCK_STREAM, 0); 
   if (m_socket < 0) {
    ERR("Failed to open socket");
  }
  
  if (bind(m_socket, reinterpret_cast<sockaddr*>(&address_structure), sizeof(address_structure)) < 0) {
  	ERR("Failed to bind socket to the address");
  }
  linger linger_opt = { 1, 0 };  
  setsockopt(m_socket, SOL_SOCKET, SO_LINGER, &linger_opt, sizeof(linger_opt));
  
  listen(m_socket, 20);
  m_api_impl = new ServerApi();
}


void Server::run() {
  std::thread worker_thread(&Server::receiverThread, this); 
  worker_thread.detach();
  printHelp();
  
  char command[5];
  do {
   printf("server@server:");
   scanf("%s", command);
 } while (evaluate(command));
}

Server::~Server() {
  delete m_api_impl;
}


const char* HELP = "help";
const char* STOP = "stop";

bool Server::evaluate(char* command) {
  if (strcmp(HELP, command) == 0) {
   printHelp();
  } else if (strcmp(STOP, command) == 0) {
    stopServer();
    return false;
  } else {
   WRN("Undefined command: %s", command);
  }
  return true;
}

void Server::printHelp() {
  printf("Commands:\n\thelp - print this help \
                   \n\tstop - send terminate signal to all peers and stop server\n");
}

void Server::stopServer() {
    m_is_stopped = true;
}


void Server::receiverThread() {

  while ( !m_is_stopped ) {
  	
    sockaddr_in peer_address_structure;
    socklen_t peer_address_structure_size;
    
    int peer_socket = accept(m_socket, reinterpret_cast<sockaddr*>(&peer_address_structure), &peer_address_structure_size);
    if (peer_socket < 0) {
      ERR("Failed to open new socket for data transfer");
      continue;
    }
    
    std::thread worker_thread(&Server::handleClient, this, peer_socket);
    worker_thread.detach();

  }
}


Request Server::getRequest(int socket, bool* is_closed) {
  
  char buffer[4096]; 
  memset(buffer, 0, sizeof(buffer));
  
  int nbytes  = recv(socket, buffer, sizeof(buffer), 0);
  if (nbytes == 0) {
  	*is_closed = true;
  	return Request::EMPTY;
  }
  //из сырого буфера получает настроящий request 
  Request request = m_parser.parseRequest(buffer, nbytes);
  return request;
}

void Server::handleClient(int socket) {
	while ( !m_is_stopped ) {
		bool is_closed = false;
		Request request = getRequest(socket, &is_closed);
		if (is_closed) {
		  close(socket);
		  return;
		}
		Method method = getMethod(request.startline.method);
		Path path = getPath(request.startline.path);

    		m_api_impl->setSocket(socket);
		switch (path) {
			//enum Path { LOGIN, REGISTER, MESSAGE, LOGOUT, SWITCH_CHANNEL }
			case Path::LOGIN:
			  switch (method) {
			    case Method::GET:
			       m_api_impl->sendLoginForm();
				DBG("sendLoginForm");
			       break;
			    case Method::POST:
				{
	          		int id = UNKNOWN_ID;
	   	 		StatusCode login_status = m_api_impl->login(request.body, id);
	   	  		m_api_impl->sendStatus(login_status, id);
				DBG("login");
	       		       }
	       		      break;
	      		}
			break;
	    case Path::REGISTER:
	      switch (method) {
	      	case Method::GET:
	      		m_api_impl->sendRegistrationForm();
			DBG("sendRegistrationForm");
	      		break;
	      	
	      	case Method::POST: 
                {
	      		int id = UNKNOWN_ID;
	      		StatusCode registrate_status = m_api_impl->registrate(request.body, id);
	      		m_api_impl->sendStatus(registrate_status, id);
			DBG("registrate");
	      	}
                break;
	      }  
		break;
	    case Path::MESSAGE:
	      switch (method) {
	      	case Method::POST:
                {
	      		int id = UNKNOWN_ID;
	      		StatusCode message_status = m_api_impl->message(request.body, id);
			DBG("message");
	      	 	m_api_impl->sendStatus(message_status, id);
	      	}
                break;
	      } 
		break;
      case Path::LOGOUT:
        switch (method) {
        	case Method::POST: 
		{
        		int id = UNKNOWN_ID;
        		//TODO
        	}
		break;
        }
	    break;
	  }
	}
}


//enum Method { GET, POST, PUT, DELETE }
Method getMethod(const std::string& raw_method) {

  if (strcmp("GET", raw_method.c_str()) == 0) {
    return Method::GET;
  } else if (strcmp("POST", raw_method.c_str()) == 0) {
    return Method::POST;
  } else if (strcmp("PUT", raw_method.c_str()) == 0) {
    return Method::PUT;
  } else if (strcmp("DELETE", raw_method.c_str()) == 0) {
    return Method::DELETE;
  } else {
    WRN("Wrong method!");
  }
  /*
  если это строка “GET” - return Method::GET;
  если это строка “POST” - return Method::POST;
    ...
  */
}
//enum Path { LOGIN, REGISTER, MESSAGE, LOGOUT, SWITCH_CHANNEL }
Path getPath(const std::string& raw_path) {

  if (strcmp("/login", raw_path.c_str()) == 0) {
    return Path::LOGIN;
  } else if (strcmp("/register", raw_path.c_str()) == 0) {
    return Path::REGISTER;
  } else if (strcmp("/message", raw_path.c_str()) == 0) {
    return Path::MESSAGE;
  } else if (strcmp("/logout", raw_path.c_str()) == 0) {
    return Path::LOGOUT;
  } else if (strcmp("/switch_channel", raw_path.c_str()) == 0) {
    return Path::SWITCH_CHANNEL;
  } else {
    WRN("Wrong path!");
  }
	/*
	“/login” - return Path::LOGIN;
	...
	*/
}


