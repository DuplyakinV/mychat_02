#ifndef MY_CHAT_SERVER_API__H_
#define MY_CHAT_SERVER_API__H_

#include "all.h"

#include "participant.h"
#include <unordered_map>
#include "client/client.h"
#include "database.h"

class ServerApi {
public:
  ServerApi();
  ~ServerApi();

  void sendLoginForm();
  void sendRegistrationForm();
  //void sendMessage(const Message& message);
  void sendStatus(StatusCode status, int id);
 
  StatusCode login(const std::string& json, int& id);
  StatusCode registrate(const std::string& json, int& id);
  StatusCode message(const std::string& json, int& id);
  
  StatusCode loginPeer(const LoginForm& form, int& id);
  int registerPeer(const RegistrationForm& form);
  void broadcast(const Message& message);
  bool authenticate(const std::string& db_password, const std::string& input_password);
  void doLogin(int id, const std::string& login);
  void setSocket(int socket) { m_socket = socket; }


private: 
  std::unordered_map<int, Participant> m_peers; 
  Database* m_database; 
  int m_socket;

  bool isAuthorized(int id) const;
};

#endif 
