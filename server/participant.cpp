#include "participant.h"

Participant::Participant(int id, const std::string& name)
  :m_id(id)
  ,m_name(name) {

  }

void Participant::setSocket(int socket) { m_socket = socket; }

int Participant::getSocket() const{ return m_socket; }
const std::string& Participant::getName() const {return m_name; }
int Participant::getId() const { return m_id; }




  
