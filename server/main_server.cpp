#include "server.h"
#include "logger.h"

int main(int argc, char** argv) {
  DBG("Server");

  int port = 9000;
  if (argc > 1) {
    port = std::atoi(argv[1]);
  }

  Server server(port);
  server.run();

  DBG("Server END");
  return 0;
}

