#include "server_api.h"
#include <sstream>

/*SERVER IMPLEMENTTION

Что делает сервер, когда получает один из этих методов от клиента? 

CLIENT API

GET /login
POST /login
GET /register
POST /register
POST /message

*/

/*
Response:

HTTP[/VERSION] [CODE] [MESSAGE]
Header1: value1
Header2: value2
...

body
*/

static const char* STANDARD_HEADERS = "Server: ChatServer\r\nContent-Type: application/json";
static const char* CONTENT_LENGTH_HEADER = "Content-Length: ";
static const char* CONNECTION_CLOSE_HEADER = "Connection: close";

ServerApi::ServerApi() {
    m_database = new Database();
}

ServerApi::~ServerApi() {
   delete m_database;
}

void ServerApi::sendStatus(StatusCode status, int id) {
  std::ostringstream oss, json;
  oss << "HTTP/1.1 ";
  switch (status) {
    case StatusCode::SUCCESS:
      oss << "200 OK\r\n" << STANDARD_HEADERS << "\r\n";
      break;
    case StatusCode::WRONG_PASSWORD:
      oss << "403 Wrong password\r\n" << STANDARD_HEADERS << "\r\n";
      break;
    case StatusCode::NOT_REGISTERED:
      oss << "404 Not registered\r\n" << STANDARD_HEADERS << "\r\n";
      break;
    case StatusCode::ALREADY_REGISTERED:
      oss << "409 Already registered\r\n" << STANDARD_HEADERS << "\r\n";
      break;
    case StatusCode::INVALID_FORM:
      oss << "400 Invalid form\r\n" << STANDARD_HEADERS << "\r\n";
      break;
    case StatusCode::UNAUTHORIZED:
      oss << "401 Unauthorized\r\n" << STANDARD_HEADERS << "\r\n";
      break;
    case StatusCode::ALREADY_LOGGED_IN:
      oss << "409 Already logged in\r\n" << STANDARD_HEADERS << "\r\n";
      break;
    case StatusCode::UNKNOWN:
      oss << "500 Internal server error\r\n" << STANDARD_HEADERS << "\r\n";
      break;
    default:
      return;
  }

  auto it_peer = m_peers.find(id);

  json << "{\"code\":" << static_cast<int>(status)
       << ",\"id\":" << id << "}";
  oss << CONTENT_LENGTH_HEADER << json.str().length() << "\r\n\r\n"
      << json.str();
  MSG("Response: %s", oss.str().c_str());
  send(m_socket, oss.str().c_str(), oss.str().length(), 0);
}

//Ответ сервера на запрос GET /login со стороны клиента 
//GET /login <--- http://127.0.0.1:9000/login
void ServerApi::sendLoginForm() {
  std::ostringstream json;
  json << "{\"login\":\"\",\"password\":\"\"}";     // {“login”:””,”password”:””}

  std::ostringstream oss;
  oss << "HTTP/1.1 200 OK\r\n"
      << "Host: 127.0.0.1:9000\r\n"
      << "Server: ChatServer\r\n"
      << "Content-Type: application/json\r\n"
      << "Content-Length: " << json.str().length() << "\r\n\r\n"
      << json.str();

    send(m_socket, oss.str().c_str(), oss.str().length(), 0);
}


//Сервер возвращает id клента 
//Ответ сервера на запрос POST /login со стороны клиента
//POST /login
//{“login”:”Vlad”,”password”:”qwerty”}

StatusCode ServerApi::login(const std::string& json, int& id) {

   // login peer,  obtain peer id
  DBG("Login: %s", json.c_str());
 
  rapidjson::Document document;
  document.Parse(json.c_str());
  
  if (document.IsObject() &&
    document.HasMember("login") && document["login"].IsString() &&
    document.HasMember("password") && document["password"].IsString()) {

    std::string login = document["login"].GetString();
    std::string password = document["password"].GetString();
    LoginForm form(login, password);
    return loginPeer(form, id);
    
  } 
  ERR("Invalid login form");
   return StatusCode::INVALID_FORM;
}

StatusCode ServerApi::loginPeer(const LoginForm& form, int& id) {
  id = UNKNOWN_ID;
  
  Peer peer = m_database->getPeerByLogin(form.getLogin(), id);
  const std::string& input_password = form.getPassword();
  std::string& db_password = peer.password;
  DBG("Password: input = %s, db = %s", input_password.c_str(), db_password.c_str());

  if (id != UNKNOWN_ID) {
    if (authenticate(db_password, input_password)) {
      if (m_peers.find(id) != m_peers.end()) {
        WRN("Already logged in");
        return StatusCode::ALREADY_LOGGED_IN;
      } else {
        doLogin(id, peer.login);
        INF("Success login");
        return StatusCode::SUCCESS;
      }
    } else {
      WRN("Wrong password");
      return StatusCode::WRONG_PASSWORD;
    }
  }

  ERR("Error not registered");
  return StatusCode::NOT_REGISTERED;
}

bool ServerApi::authenticate(const std::string& db_password, const std::string& input_password) {
  return (db_password.compare(input_password) == 0);
}


void ServerApi::doLogin(int id, const std::string& login) {
  Participant participant(id, login);
  participant.setSocket(m_socket);
  m_peers.insert(std::pair<int, Participant>(id, participant));
}


//Ответ сервера на запрос GET /register со стороны клиента 
//GET /register
//<---{“login”:””,”email”:””,”password”:””}
void ServerApi::sendRegistrationForm() {
  std::ostringstream json;
  json << "{\"login\":\"\",\"email\":\"\",\"password\":\"\"}";

  std::ostringstream oss;
  oss << "HTTP/1.1 200 OK\r\n"
      << "Host:127.0.0.1:9000\r\n" 
  	  << "Server: ChatServer\r\n"
      << "Content-Type: application/json\r\n"
      << "Content-Length: " << json.str().length() << "\r\n\r\n"
      << json.str();

    send(m_socket, oss.str().c_str(), oss.str().length(), 0);
}

//Ответ сервера на запрос POST /message со стороны клиента 
//POST /register
StatusCode ServerApi::registrate(const std::string& json, int& id) {
   // register peer, obtain peer id
  DBG("Registrate: %s", json.c_str());
  rapidjson::Document document;
  document.Parse(json.c_str());

  // RegistrationForm json:  {“login”:””,”email”:””,”password”:””}
  if (document.IsObject() &&
    document.HasMember("login") && document["login"].IsString() &&
    document.HasMember("email") && document["email"].IsString() &&
    document.HasMember("password") && document["password"].IsString()) {

    std::string login = document["login"].GetString();
    std::string email = document["email"].GetString();
    std::string password = document["password"].GetString();
    RegistrationForm form(login, email, password);
    id = registerPeer(form);
    DBG("Registered with id: %i", id);
    if (id == UNKNOWN_ID) {
      WRN("Already registered");
      return StatusCode::ALREADY_REGISTERED;
    } else {
      INF("Success registered");
      return StatusCode::SUCCESS;
    }
  }

  ERR("Invalide register form");
  return StatusCode::INVALID_FORM;
}

int ServerApi::registerPeer(const RegistrationForm& form) {
  int id = UNKNOWN_ID; 
  m_database->getPeerByLogin(form.getLogin(), id);
    if (id == UNKNOWN_ID) {
      Peer new_peer(form.getLogin(), form.getPassword(), form.getEmail());
      id = m_database->addPeer(new_peer);
      doLogin(id, new_peer.login);
      return id;
    } else {

  }

  return UNKNOWN_ID;
}


//Ответ сервера на запрос POST /register со стороны клиента 
//POST /message
//посылает всем message 
StatusCode ServerApi::message(const std::string& json, int& id) {

  Message messageObj;

  rapidjson::Document document;
  document.Parse(json.c_str());

  if (document.IsObject() &&
    document.HasMember("id") && document["id"].IsInt() &&
    document.HasMember("login") && document["login"].IsString() &&
    document.HasMember("channel") && document["channel"].IsInt() &&
    document.HasMember("dest_id") && document["dest_id"].IsInt() &&
    document.HasMember("timestamp") && document["timestamp"].IsInt64() &&
    document.HasMember("message") && document["message"].IsString() ) {
    
    int id = document["id"].GetInt();  

    if (!isAuthorized(id)) {
      return StatusCode::UNAUTHORIZED;
    }

    std::string login = document["login"].GetString();
    int channel = document["channel"].GetInt(); 
    int dest_id = document["dest_id"].GetInt(); 
    std::string message = document["message"].GetString();

    messageObj.setId(id);
    messageObj.setLogin(login);
    messageObj.setChannel(channel);
    messageObj.setDest_id(dest_id);
    messageObj.setMessage(message);

    broadcast(messageObj);
    return StatusCode::SUCCESS;
  }

  return StatusCode::INVALID_FORM;
}

void ServerApi::broadcast(const Message& message) { 
  for ( auto i = m_peers.begin(); i != m_peers.end(); ++i ) {
    if  (message.getId() != i->second.getId()) {
      
      
      
      std::string json = message.toJson();
      std::ostringstream oss;
      oss << "HTTP/1.1 200 OK\r\n"
          << "Server: Chat-Server version 1.0\r\n"
          << "Content-Type: application/json\r\n"
          << "Content-Length: " << json.length()
          << "\r\n\r\n" << json;
      send(i->second.getSocket(), oss.str().c_str(), oss.str().length(), 0);
    }
  }
}

bool ServerApi::isAuthorized(int id) const {
  return m_peers.find(id) != m_peers.end();
}

