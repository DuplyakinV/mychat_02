#ifndef MY_CHAT_PARTICIPANT__H__
#define MY_CHAT_PARTICIPANT__H__

#include <string>

class Participant {
public:
  Participant(int id, const std::string& name);

  void setSocket(int socket);

  int getSocket() const;
  const std::string& getName() const;
  int getId() const; 
private:
   int m_id;
   std::string m_name;
   int m_socket;
};

#endif
