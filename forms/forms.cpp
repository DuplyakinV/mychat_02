#include "forms.h"
#include <sstream>

//login
std::string LoginForm::toJson() const {
  std::ostringstream json;
  json << "{\"login\":\"" << m_login << "\",\"password\":\"" << m_password << "\"}";  
  return json.str();
}

//registration
std::string RegistrationForm::toJson() const {
  std::ostringstream json;
  json << "{\"login\":\"" << m_login << "\",\"email\":\"" << m_email << "\",\"password\":\"" << m_password << "\"}";  
  return json.str();
}

// message
std::string Message::toJson() const {
  std::ostringstream json;
  json << "{\"id\":" << m_id << ",\"login\":\"" << m_login << "\",\"channel\":" << m_channel << ",\"dest_id\":" << m_dest_id << ",\"timestamp\":" << m_timestamp << ",\"message\":\"" << m_message << "\"}"; 
  return json.str();
}

