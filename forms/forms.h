#ifndef MY_CHAT_FORMS_01__H__
#define MY_CHAT_FORMS_01__H__

#include <string>


class Message {
public:
  std::string toJson() const;

  void setId(int id) { m_id = id; }
  void setLogin(const std::string& login) { m_login = login; }
  void setChannel(int channel) { m_channel = channel; }
  void setDest_id(int dest_id) { m_dest_id = dest_id; }
  void setTimestamp(uint64_t timestamp) { m_timestamp = timestamp; }
  void setMessage(const std::string& message) { m_message = message; }
  

  int getId() const { return m_id; };
  const std::string& getLogin() const { return m_login; }
  int getChannel(int channel) const { return m_channel; };
  int getDest_id(int dest_id) const { return m_dest_id; };
  uint64_t getTimestamp(uint64_t timestamp) const { return m_timestamp; };
  const std::string& getMessage() const { return m_message; }


private:
  int m_id;
  std::string m_login;
  int m_channel;
  int m_dest_id;
  uint64_t m_timestamp;
  std::string m_message;
};



///////////////////////////////////////////////////////////////////////////////////////////////////////////////
//login:

class LoginForm {
public:
  LoginForm(const std::string& login, const std::string& password)
    :m_login(login), 
     m_password(password) {}

  std::string toJson() const;
  void setLogin(const std::string& login) { m_login = login; }
  void setPassword(const std::string& pass) { m_password = pass; }
  const std::string& getLogin() const { return m_login; }
  const std::string& getPassword() const { return m_password; }

protected:
  std::string m_login;
  std::string m_password;
};

//registration:

class RegistrationForm {
public:
  RegistrationForm (const std::string& login, const std::string& email, const std::string& password)
    :m_login(login), 
     m_email(email),
     m_password(password) {}

  std::string toJson () const;
  void setLogin(const std::string& login) { m_login = login; }
  void setEmail(const std::string& email) { m_email = email; }
  void setPassword(const std::string& password) { m_password = password; }
  const std::string& getLogin() const { return m_login; } 
  const std::string& getEmail() const { return m_email; } 
  const std::string& getPassword() const { return m_password; } 

private:
  std::string m_login;
  std::string m_email;
  std::string m_password;
};

//-----------------------------------------------------------------------------



#endif 
