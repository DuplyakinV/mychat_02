#ifndef MY_CHAT_CLIENT__H_
#define MY_CHAT_CLIENT__H_

#include "all.h"

#include "client_api.h"
#include "forms/forms.h"
#include "client/client_api.h"
#include "parser/x_my_parser.h"
#include "logger.h"

class Client {
public:
  Client(const std::string& ip_address, const std::string& port);
  ~Client();

  void init();
  void locateServer(const std::string& str);
  bool readConfiguration(const std::string& config_file);
  void run();

private:
  int m_id;
  std::string m_name;
  int m_channel;
  int m_socket;
  std::string m_ip_address;
  std::string m_port;
  XMyParser m_parser;
  

  ClientApi* m_api_impl; 


  
 
  Response getResponse(int socket, bool* is_closed);//класс Response содержит ответ - это обёртка на recieve 
  /* Login */
  void requestLoginForm();//previous name: void getLoginForm();
  void fillLoginForm(LoginForm* form);
  void tryLogin(LoginForm& form);
  //обработчики ошибок
  void onLogin();  // SUCCESS
  void onAlreadyLoggedIn();
  void onNotRegistered(); //NOT_REGISTERED 
  void onWrongPassword(LoginForm& form);//WRONG_PASSWORD

  /* Register */
  void requestRegistrationForm();
  void fillRegistrationForm(RegistrationForm* form);
  void tryRegister(RegistrationForm& form);
  ////обработчики ошибок
  void  onRegister();   // SUCCESS
  void onAlreadyRegistered(); //ALREADY_REGISTERED 
  

  /* Error */
  //общие ошибки 
  void onInvalidForm(); //INVALID_FORM (тело запроса является некорректным с точки зрения json)
  void onUnathorized(); //UNAUTHORIZED 
  void onInvalidQuery(); //INVALID_QUERY (параметры запроса являются некорректными с точки зрения json)

  /* Message */
  void startChat();//TODO
  void receiverThread(int sockfd);//TODO
  bool checkStatus(const std::string& json);

  /* Menu */
  void mainMenu();
};

#endif  // CLIENT__H_
