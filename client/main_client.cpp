#include "client/client.h"
#include "logger.h"


int main(int argc, char** argv) {
  DBG("Client");
  std::string ip_address = "127.0.0.1";
  std::string port = "80";
  std::string config_file = "../data/server.cfg";

  if (argc > 1) {
    ip_address = argv[1];
    if (argc > 2) {
      port = std::atoi(argv[2]);
      if (argc > 3) {
        char buffer[256];
        strncpy(buffer, argv[3], strlen(argv[3]));
        config_file = std::string(buffer); 
      }
    }
  }

  DBG("Configuration from file: %s", config_file.c_str());
  Client client(ip_address, port);
  DBG("Trying to lacate server:");
  
  client.readConfiguration(config_file);
  DBG("Trying to init:");
  client.init();
  DBG("Trying to run client:");
  client.run();

  DBG("Client END");
  return 0;
}
