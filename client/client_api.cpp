#include "client_api.h"

ClientApi::ClientApi(const int socket, int port, int ip_address)
: m_socket(socket)
, m_port(port)
, m_ip_address(ip_address) {
}

ClientApi::~ClientApi() {
  close(m_socket);
}

//login:
//client<---{“login”:””,”password”:””}//тело того, что придёт от сервера на этот запрос getLoginForm
//это запрос клиента к серверу: "Пришли мне форму логина". Сервер присылает пустую форму клиенту.
void ClientApi::getLoginForm() {
  std::ostringstream oss;
  oss << "GET /login HTTP/1.1\r\n"
      //<< “Host: 127.0.0.1:9000\r\n\r\n”;
      << "Host: " 
      << std::to_string(m_ip_address) 
      << ":"  
      << std::to_string(m_port) 
      << "\r\n\r\n";

  send(m_socket, oss.str().c_str(), oss.str().length(), 0);
}

//client--->{“login”:”Vlad”,”password”:”qwerty”}//тело того, что отправит клиент
//клиент отправляет на сервер заполненную форму логина
void ClientApi::sendLoginForm(const LoginForm& form) {
  std::ostringstream oss;
  //oss << “POST /login HTTP/1.1\r\n”
	//    << “Host: 127.0.0.1:9000\r\n\r\n”

  oss << "POST /login HTTP/1.1\r\n"
      << "Host: " 
      << std::to_string(m_ip_address) 
      << ":"  
      << std::to_string(m_port) 
      << "\r\n\r\n"
      << form.toJson();
  send(m_socket, oss.str().c_str(), oss.str().length(), 0);
}


//registration
//client<---{“login”:””,”email”:””,”password”:””}
void ClientApi::getRegistrationForm() {
  std::ostringstream oss;
  //oss << “GET /register HTTP/1.1\r\n”
  //    << “Host: 127.0.0.1:9000\r\n\r\n”;
  oss << "GET /register HTTP/1.1\r\n"
      << "Host: " 
      << std::to_string(m_ip_address) 
      << ":"  
      << std::to_string(m_port) 
      << "\r\n\r\n";    
  
  send( m_socket, oss.str().c_str(), oss.str().length(), 0);
}

//client--->{“login”:”Vlad”,”email”:”vlad@yandex.ru”,”password”:”qwerty”}
void ClientApi::sendRegistrationForm(const RegistrationForm& form) {
  std::ostringstream oss;
  //oss << “POST /register HTTP/1.1\r\n”
  //<< “Host: 127.0.0.1:9000\r\n\r\n”  
  oss << "POST /register HTTP/1.1\r\n"
      << "Host: " 
      << std::to_string(m_ip_address) 
      << ":"  
      << std::to_string(m_port) 
      << "\r\n\r\n"
      << form.toJson();
  
  send( m_socket, oss.str().c_str(), oss.str().length(), 0);
}

//message:
void ClientApi::sendMessage(const Message& message) {
  std::ostringstream oss;
  //oss << “POST /message HTTP/1.1\r\n”
  //    << “Host: 127.0.0.1:9000\r\n\r\n”
  oss << "POST /message HTTP/1.1\r\n"
      << "Host: " 
      << std::to_string(m_ip_address) 
      << ":"  
      << std::to_string(m_port) 
      << "\r\n\r\n"
      << message.toJson();

  send(m_socket, oss.str().c_str(), oss.str().length(), 0);
}


bool ClientApi::readConfiguration(const std::string& config_file) {
  bool result = true;

  std::fstream fs;
  fs.open(config_file, std::fstream::in);

  if (fs.is_open()) {
    std::string line;
    
    // ip address
    std::getline(fs, line);
    int i1 = line.find_first_of(' ');
    std::string ip_address_str = line.substr(i1 + 1);
    m_ip_address = inet_addr(ip_address_str.c_str());

    // port
    std::getline(fs, line);
    int i2 = line.find_first_of(' ');
    std::string port_str = line.substr(i2 + 1);
    m_port = htons(std::atoi(port_str.c_str()));

    fs.close();
  } else {
    ERR("Failed to open configure file: %s", config_file.c_str());
    result = false;
  }

  return result;
}