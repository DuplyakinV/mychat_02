#include "client.h"

Client::Client(const std::string& ip_address, const std::string& port)
: m_ip_address(ip_address)
, m_port(port) {
}

Client::~Client() {
  close(m_socket);
  delete m_api_impl;
}

void Client::init() {

  
  //second variant(старый способ - вручную заполнить структуру sockaddr_in, которая используется параметром в connect() )
  /*
  sockaddr_in {
    sin_family;
    sin_addr.s_addr;
    sin_port;
    sin_zero[8];
  }
  */
  sockaddr_in server_info;
  memset(&server_info, 0, sizeof(server_info));
  server_info.sin_family = AF_INET;
  server_info.sin_addr.s_addr = inet_addr(m_ip_address.c_str());
  server_info.sin_port = htons(atoi(m_port.c_str()));
  m_socket = socket(AF_INET, SOCK_STREAM, 0);
  connect(m_socket, reinterpret_cast<sockaddr*>(&server_info), sizeof(server_info));

  m_api_impl = new ClientApi(m_socket, atoi(m_ip_address.c_str()), atoi(m_port.c_str()));
}


bool Client::readConfiguration(const std::string& config_file) {
  bool result = true;
  std::fstream fs;
  fs.open(config_file, std::fstream::in);

  if (fs.is_open()) {
    std::string line;
    // ip address
    std::getline(fs, line);
    int i1 = line.find_first_of(' ');
    m_ip_address = line.substr(i1 + 1);
    DBG("IP address: %s", m_ip_address.c_str());
    // port
    std::getline(fs, line);
    int i2 = line.find_first_of(' ');
    m_port = line.substr(i2 + 1);
    DBG("Port: %s", m_port.c_str());
    fs.close();
  } else {
    ERR("Failed to open configure file: %s", config_file.c_str());
    result = false;
  }
  return result;
}


void Client::run() {
  // 1. listen for incomming messages
  // 2. send messages
  //std::thread t(listener); --- лучше сделать это в методе StartChat()
  //t.detach(); 
  mainMenu();    
}

/* Menu */
// login, register, exit
void Client::mainMenu() {
  std::string command;
  printf("---------- Main ----------\n\n         login\n\n       register\n\n          exit\n\nEnter command: ");
  while(std::cin >> command) {
    if (strcmp("login", command.c_str()) == 0) {
      requestLoginForm();
      return;
    } else if (strcmp("register", command.c_str()) == 0) {
      requestRegistrationForm();
      return;
    } else if (strcmp("exit", command.c_str()) == 0) {
      break;
    } else {
      WRN("Wrong command!");
    }
  }  
}


Response Client::getResponse(int socket, bool* is_closed) {
  char buffer[4096];
  memset(buffer, 0, sizeof(buffer));
  int nbytes  = recv(socket, buffer, sizeof(buffer), 0);
  if (nbytes == 0) {
    *is_closed = true;
    return Response::EMPTY;
  }
  Response response = m_parser.parseResponse(buffer, nbytes);
  return response;
}


//------------------------------------------------------------------------------------------------------------------
//login:
void Client::requestLoginForm() { 
  m_api_impl->getLoginForm();  
  bool is_stopped = false;
  Response response = getResponse(m_socket, &is_stopped);    
  std::string json = response.getBody();   
  //парсим
  rapidjson::Document document;
  document.Parse(json.c_str());

  // LoginForm json:  {“login”:””,”password”:””}
  if (document.IsObject() &&
    document.HasMember("login") && document["login"].IsString() &&
    document.HasMember("password") && document["password"].IsString()) {

    std::string login = document["login"].GetString();
    std::string password = document["password"].GetString();
    LoginForm form(login, password);
    fillLoginForm(&form);
    tryLogin(form);
    
  } else {
    
  }
}

void Client::fillLoginForm(LoginForm* form) {
  std::string login;
  std::string password;
  printf("login: ");
  std::cin >> login;
  printf("password: ");
  std::cin >> password;
  form->setLogin(login);
  form->setPassword(password);
}

void Client::tryLogin(LoginForm& form) {
  m_api_impl-> sendLoginForm(form); 
  bool is_stopped = false;
  Response response = getResponse(m_socket, &is_stopped); 
  // StatusCode:    {“code”:x,”id”:y} 
  std::string json = response.getBody(); 
  rapidjson::Document document;
  document.Parse(json.c_str());

  if(document.IsObject()&&
    document.HasMember("code") && document["code"].IsInt() && 
    document.HasMember("id") && document["id"].IsInt()) {

    int code =  document["code"].GetInt();
    StatusCode status = static_cast<StatusCode>(code);

    switch(status) {
      case StatusCode::SUCCESS:
        m_id = document["id"].GetInt();
        m_name = form.getLogin();
        onLogin();
        break;
      case StatusCode::ALREADY_LOGGED_IN: 
        onAlreadyLoggedIn();
        break;
      case StatusCode::NOT_REGISTERED:
        onNotRegistered();
        break;
      case StatusCode::WRONG_PASSWORD: 
        onWrongPassword(form);
        tryLogin(form);
        break;
      case StatusCode::INVALID_FORM:
        ERR("Invalid json response body");
        break;
    }
  }
}

//login(errors):
void Client::onLogin() {
  startChat();//TODO здесь будет вестись основная работа по переписке
}

void Client::onAlreadyLoggedIn() {
  WRN("Already logged in");
  mainMenu();
}
void Client::onNotRegistered() {
  WRN("Not registered");
  requestRegistrationForm();
}

void Client::onWrongPassword(LoginForm& form) {
  WRN("Wrong password");
  printf("password:");
  std::string password;
  std::cin >> password;  
  form.setPassword(password);
}

//-----------------------------------------------------------------------------------------------------------------
//registration

void Client::requestRegistrationForm() { 
  m_api_impl->getRegistrationForm();
  bool is_stopped = false;
  Response response = getResponse(m_socket, &is_stopped);   
  std::string json = response.getBody();

  rapidjson::Document document;
  document.Parse(json.c_str());

  // RegistrationForm json:  {“login”:””,”email”:””,”password”:””}
  if (document.IsObject() &&
    document.HasMember("login") && document["login"].IsString() &&
    document.HasMember("email") && document["email"].IsString() &&
    document.HasMember("password") && document["password"].IsString()) {

    std::string login = document["login"].GetString();
    std::string email = document["email"].GetString();
    std::string password = document["password"].GetString();
    RegistrationForm form(login, email, password);
    fillRegistrationForm(&form);
    tryRegister(form);
    
  } else {
    //ERROR - Invalid Response Body
  }
}

void Client::fillRegistrationForm(RegistrationForm* form) {
  std::string login;
  std::string email;
  std::string password;
  printf("login: ");
  std::cin >> login;
  printf("email: ");
  std::cin >>  email;
  printf("password: ");
  std::cin >>  password;
  form->setLogin(login);
  form->setEmail(email);
  form->setPassword(password);
};

/*
enum StatusCode {
   UNKNOWN = -1, SUCCESS = 0, WRONG_PASSWORD = 1, NOT_REGISTERED = 2, 
   ALREADY_LOGGED_IN = 3, ALREADY_REGISTERED = 4, INVALID_FORM = 5,
   INVALID_QUERY = 6, UNAUTHORIZED = 7
};
*/
void Client::tryRegister(RegistrationForm& form) {
  bool is_stopped = false;
  m_api_impl->sendRegistrationForm(form); 
  Response response = getResponse(m_socket, &is_stopped); 
  // StatusCode:    {“code”:x,”id”:y} 
  std::string json = response.getBody(); 
  rapidjson::Document document;
  document.Parse(json.c_str());

  if(document.IsObject()&&
    document.HasMember("code") && document["code"].IsInt() && 
    document.HasMember("id") && document["id"].IsInt()) {

    int code =  document["code"].GetInt();
    StatusCode status = static_cast<StatusCode>(code);

    switch(status) {
      case StatusCode::SUCCESS:
        //TODO register peer
        m_id = document["id"].GetInt();
        m_name = form.getLogin();
        onRegister();
        break;
      case StatusCode::ALREADY_REGISTERED: 
        onAlreadyRegistered();
        break;
      case StatusCode::INVALID_FORM:
        ERR("Invalid json response body");
        break;
    }
  }
}

//registrate(errors):
void Client::onRegister() {
  startChat();//TODO здесь будет вестись основная работа по переписке
}

void Client::onAlreadyRegistered() {
  WRN("Already Registered");
  mainMenu();
  //или можно также requestRegistrationForm(); 
}
//-----------------------------------------------------------------------------------------------------------------
//message


void Client::startChat() {
  std::thread receiver_thread(&Client::receiverThread, this, m_socket);
  receiver_thread.detach();

  std::string buffer;
  std::cin.ignore();
  while (std::getline(std::cin, buffer)) {
    Message message;
    message.setId(m_id);
    message.setTimestamp(0);
    message.setLogin(m_name);
    message.setDest_id(0);
    message.setChannel(m_channel);
    message.setMessage(buffer);
    m_api_impl->sendMessage(message);
  }
}




void Client::receiverThread(int sock) {
  bool is_stopped = false;
  while (!is_stopped) {
    Response response = getResponse(sock, &is_stopped);
    
    //  {“code”:0, “id”:1001}
    if (checkStatus(response.getBody())) {
      continue;
    }

     // {“id”:1000, “login”:”Vlad”, … , “message”:”Hello, World”}
    Message message;

    std::string json = response.getBody(); 
    rapidjson::Document document;
    document.Parse(json.c_str());
    /*

    поля message: 
    int m_id;
    std::string m_login;
    int m_channel;
    int m_dest_id;
    uint64_t m_timestamp;
    std::string m_message;
    */

    
     //……….. ??? (parse json:   response.getBody())
     //message.set(...)
     //….

    if ( document.IsObject() &&
      document.HasMember("id") && document["id"].IsInt() &&
      document.HasMember("login") && document["login"].IsString() &&
      document.HasMember("channel") && document["channel"].IsInt() &&
      document.HasMember("dest_id") && document["dest_id"].IsInt() &&
      document.HasMember("timestamp") && document["timestamp"].IsInt64() &&
      document.HasMember("message") && document["message"].IsString() ) {
      
      int id = document["id"].GetInt();  
      std::string login = document["login"].GetString();
      int channel = document["channel"].GetInt(); 
      int dest_id = document["dest_id"].GetInt(); 
      std::string str_message = document["message"].GetString();

      message.setId(id);
      message.setLogin(login);
      message.setChannel(channel);
      message.setDest_id(dest_id);
      message.setMessage(str_message);

    } else {
      //return StatusCode::INVALID_FORM;
      return;
      //ERROR - Invalid Response Body
    }  


    // Login :: timestamp :: message
    std::chrono::time_point<std::chrono::system_clock> end =  std::chrono::system_clock::now();
    std::time_t end_time = std::chrono::system_clock::to_time_t(end);
    std::string timestamp(std::ctime(&end_time));
    int i1 = timestamp.find_last_of('\n');
    timestamp = timestamp.substr(0, i1);

    printf("%s :: %s :: %s\n", message.getLogin().c_str(), timestamp.c_str(), message.getMessage().c_str() ); 
  }

}


bool Client::checkStatus(const std::string& json) {
  rapidjson::Document document;
  document.Parse(json.c_str());

  return (document.IsObject()&&
    document.HasMember("code") && document["code"].IsInt() );
}


Method getCode(const std::string& raw_code) {
  //TODO
}

Method getMessage(const std::string& raw_message) {
  //TODO
}




//-----------------------------------------------------------------------------------------------------------------
//other errors
void Client::onInvalidForm() {
  WRN("Invalid json body");
  mainMenu();  
}

void Client::onUnathorized() {
  WRN("onUnathorized");
  mainMenu();
}

void Client::onInvalidQuery() {
  WRN("Invalid path");
  mainMenu();
}

