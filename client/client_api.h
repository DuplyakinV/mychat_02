#ifndef MY_CHAT_CLIENT_API__H_
#define MY_CHAT_CLIENT_API__H_

#include "all.h"

#include "forms/forms.h"

/*
CLIENT API

GET /login
POST /login
GET /register
POST /register
POST /message
*/

class ClientApi {
public:
  ClientApi(const int socket, int port, int ip_address);
  ~ClientApi();
  //login
  void getLoginForm();
  void sendLoginForm(const LoginForm& form);
  //registration
  void getRegistrationForm();
  void sendRegistrationForm(const RegistrationForm& form);
  //message
  void sendMessage(const Message& message);
  //get server ip_address and port 
  bool readConfiguration(const std::string& config_file);
private:
  int m_socket;
  int m_port;
  int m_ip_address;
};

#endif






