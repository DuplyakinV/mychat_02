# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/home/vlad/Prog/net/04/dbase/database.cpp" "/home/vlad/Prog/net/04/build/dbase/CMakeFiles/database.dir/database.cpp.o"
  "/home/vlad/Prog/net/04/dbase/peer.cpp" "/home/vlad/Prog/net/04/build/dbase/CMakeFiles/database.dir/peer.cpp.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/vlad/Prog/net/04/build/sqlite/CMakeFiles/sqlite.dir/DependInfo.cmake"
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  ".."
  "../include"
  "../data"
  "../client"
  "../server"
  "../parser"
  "../dbase"
  "../sqlite"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
