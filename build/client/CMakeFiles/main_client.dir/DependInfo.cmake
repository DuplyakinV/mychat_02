# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/home/vlad/Prog/net/04/client/client.cpp" "/home/vlad/Prog/net/04/build/client/CMakeFiles/main_client.dir/client.cpp.o"
  "/home/vlad/Prog/net/04/client/client_api.cpp" "/home/vlad/Prog/net/04/build/client/CMakeFiles/main_client.dir/client_api.cpp.o"
  "/home/vlad/Prog/net/04/client/main_client.cpp" "/home/vlad/Prog/net/04/build/client/CMakeFiles/main_client.dir/main_client.cpp.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/vlad/Prog/net/04/build/forms/CMakeFiles/forms.dir/DependInfo.cmake"
  "/home/vlad/Prog/net/04/build/parser/CMakeFiles/parser.dir/DependInfo.cmake"
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  ".."
  "../include"
  "../data"
  "../client"
  "../server"
  "../parser"
  "../dbase"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
